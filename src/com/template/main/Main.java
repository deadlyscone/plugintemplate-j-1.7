package com.template.main;

import com.template.commands.CommandRouter;
import com.template.commands.TemplateCommands;
import com.template.utils.PluginLogger;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import com.template.utils.ConsoleColor;
import com.template.utils.DataManager;

public class Main extends JavaPlugin{

	private static boolean isHalting = false;
	public static String PluginName;
	
	//******************************************************************\
	//  On Enable
	//******************************************************************\
	@Override
	public void onEnable(){
		this.PluginName = this.getName();
		// Check Files
		DataManager.CheckFiles();
		// Load Data
		DataManager.loadData();
		// Register Events
		RegisterEvents();
		
	}
	
	//******************************************************************\
	//  On Disable
	//******************************************************************\
	@Override
	public void onDisable(){
		
	}
	
	//******************************************************************\
	//  Helper Functions
	//******************************************************************\
	private void RegisterEvents(){
		if(!isHalting){
			//  Listeners
			new TemplateCommands(this);

			//  Commands
			this.getCommand("template").setExecutor(new CommandRouter(this));
		}
	}
	public static void halt(Exception reason){
		isHalting = true;
		Main plugin = (Main) Bukkit.getServer().getPluginManager().getPlugin("HoodAttack");
		if(DataManager.inDebug){
			reason.printStackTrace();
		}else{
			//  Lets write it to an error log.
		}
		PluginLogger.severe("Shutting Down! | Reason: " + reason.getMessage());
		Bukkit.getServer().getPluginManager().disablePlugin(plugin);
	}
}
