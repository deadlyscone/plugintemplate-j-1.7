package com.template.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.net.URL;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import com.template.main.Main;

public class DataManager {
	public static boolean inDebug;

		//******************************************************************\
		//  File Helper Functions
		//******************************************************************\
		private static File getPluginDataFolder(){
			return Bukkit.getPluginManager().getPlugin(Main.PluginName).getDataFolder();
		}
		
		private static File getConfigFile(String configName){
			return new File(getPluginDataFolder(), "/" + configName + ".yml");
		}

		private static void exportResource(URL resourceURL, File dest){
	        try {
	        	dest.createNewFile();
	            BufferedReader in = new BufferedReader(new InputStreamReader(resourceURL.openStream()));
	            BufferedWriter out = new BufferedWriter(new FileWriter(dest));
	            String line = "";
	            while((line = in.readLine()) != null){
	            	out.write(line);
	            	out.newLine();
	            	out.flush();
	            }
	            in.close();
	            out.close();

	        }catch(Exception e){
	        	Main.halt(e);
	        }
		}

		//******************************************************************\
		//  Loading Function
		//******************************************************************\
		public static void loadData(){
			File customYmlSkills; 
			FileConfiguration config;
			
			//Initialize configuration variables
			customYmlSkills = getConfigFile("config");
			config = YamlConfiguration.loadConfiguration(customYmlSkills);
			try{
				//Load values
				inDebug = config.getBoolean("Debug");
			}catch(Exception ex){
				//Un-Loads plugin if there is an error while retrieving the data.
				Main.halt(ex);
			}
		}
		
		//******************************************************************\
		//  Reload Function
		//******************************************************************\
		public static void reloadData(){
			saveData();
			CheckFiles();
			loadData();
		}

		//******************************************************************\
		//  Saving Function
		//******************************************************************\
		public static void saveData(){
			return;
		}

		//******************************************************************\
		//  Check Files
		//******************************************************************\
		public static void CheckFiles() {
			File pluginDataFolder = DataManager.getPluginDataFolder();
			File configFile = DataManager.getConfigFile("config");
			
			if (!pluginDataFolder.exists()){
				pluginDataFolder.mkdir();
			}
			
			if(!configFile.exists()){
				DataManager.exportResource(Main.class.getResource("/config.yml"), configFile);
			}
		}
}
