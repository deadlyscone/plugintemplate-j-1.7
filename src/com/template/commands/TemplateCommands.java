package com.template.commands;

import com.template.main.Main;
import com.template.utils.DataManager;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;


/**
 * Created by deadlyscone on 10/31/2015.
 */
public class TemplateCommands{

    private CommandSender sender;
    private String label;
    private String[] args;

    Main plugin;
    public TemplateCommands(Main plugin){
        this.plugin = plugin;
    }

    public boolean createHook(HookInfo hookInfo){
        this.sender = hookInfo.getSender();
        this.label = hookInfo.getLabel();
        this.args = hookInfo.getArgs();
        return this.hookResult();
    }

    private boolean hookResult() {
        if ((label.equalsIgnoreCase("template"))) {
            if (sender instanceof Player) {
                Player player = ((Player) sender);

                if(args.length == 1 && args[0].equalsIgnoreCase("reload") && player.hasPermission("template.admin.reload")){
                    DataManager.reloadData();
                    return true;
                }
            } else {
                // Console commands HERE
            }
        }
        return false;
    }
}
